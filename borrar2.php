<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>www.mclibre.org - Agenda - Inicio</title>
  <link href="agenda_css.css" rel="stylesheet" type="text/css" />
</head>

<body>
<h1>Agenda - Inicio</h1>
<div id="menu">
<ul>
  <li><a href="anadir.html">Anadir</a></li>
  <li><a href="listar.php">Listar</a></li>
  <li><a href="modificar.php">Modificar</a></li>
  <li><a href="buscar.php">Buscar</a></li>
  <li><a href="borrar.php">Borrar</a></li>
  <li><a href="borrar_todo.html">Borrar todo</a></li>
</ul>  
</div>

<div id="contenido">
	
<?php 

$conn = new PDO('sqlite:agenda.sqlite');

try {

	// para borrar los datos a la tabla:
	// 1. preparamos la sentencia de insercion
	$cod=$_GET['id'];
	$borrar = "DELETE from agenda WHERE id=$cod";
	
	$sentencia = $conn->prepare($borrar);
	$sentencia->execute();
		
	echo '<h2>Datos borrados</h2>';
	echo '<a href="index.html">Volver al index</a>';
	
}catch(PDOException $e){
	echo $e->getMessage();	
}
// cierra la conexion
$conn = null;

?>

</div>
	
<div id="pie">
<address>
  Este programa esta hecho por Carlos Val Cebrian
</address>

</div>
</body>
</html>




