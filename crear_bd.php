<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>www.mclibre.org - Agenda - Inicio</title>
  <link href="agenda_css.css" rel="stylesheet" type="text/css" />
</head>

<body>
<h1>Agenda - Inicio</h1>
<div id="menu">
<ul>
  <li><a href="anadir.html">Anadir</a></li>
  <li><a href="listar.php">Listar</a></li>
  <li><a href="modificar.php">Modificar</a></li>
  <li><a href="buscar.php">Buscar</a></li>
  <li><a href="borrar.php">Borrar</a></li>
  <li><a href="borrar_todo.html">Borrar todo</a></li>
</ul>  
</div>

<div id="contenido">
	
<?php

define("TAM_NOMBRE",     40);  // Tamaño del campo Nombre
define("TAM_APELLIDOS",  60);  // Tamaño del campo Apellidos
define("TAM_TELEFONO",   10);  // Tamaño del campo Teléfono
define("TAM_CORREO",     50);  // Tamaño del campo Correo

// Consulta de creación de tabla en SQLite
$consulta = "CREATE TABLE agenda (
    id INTEGER PRIMARY KEY,
    nombre VARCHAR(" . TAM_NOMBRE . "),
    apellidos VARCHAR(" . TAM_APELLIDOS . "), 
    telefono VARCHAR(" . TAM_TELEFONO . "),
    correo VARCHAR(" . TAM_CORREO . ")
    )";
	
?>

<?php

// test de acceso a sqlite
try {
	// crear una base de datos
	$conn = new PDO('sqlite:agenda.sqlite');
	//$conn = new PDO('sqlite::memory:'); // crea en una parte de la memoria una bd 
	
	$conn->exec($consulta); // ejecutamos $consulta

	echo 'La Tabla agenda ha sido creada';
	
	}catch(PDOException $e){
		echo $e->getMessage();
	}
	
?>

<br><br>
<a href="index.html">Volver al index</a>

</div>
	
<div id="pie">
<address>
  Este programa esta hecho por Carlos Val Cebrian
</address>

</div>
</body>
</html>



