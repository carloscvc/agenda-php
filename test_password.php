<?php
function generateHash($password) {
    if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
        $salt = '$2y$11$' . substr(md5(uniqid(rand(), true)), 0, 22);
        return crypt($password, $salt);
    }
}
?>

<?php

function verificar($password, $hashedPassword) {
    return crypt($password, $hashedPassword) == $hashedPassword;
}
?>

<?php

echo 'Password: Luis <br>';

$pass_hash = generateHash('Luis');

echo "Hash:", $pass_hash;
if (verificar($pass_hash, generateHash('Luis')))
	echo 'Contraseña correcta <br>';
else{
	echo 'Contraseña incorrecta <br>';
}

?>

