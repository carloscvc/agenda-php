<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
       "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <title>www.mclibre.org - Registro de usuarios 2 - Registrar nuevo usuario 1</title>
  <link href="agenda_css.css" rel="stylesheet" type="text/css" />
</head>

<body onload="document.getElementById('cursor').focus()">
<h1>Registro de usuarios - Registrar nuevo usuario 1</h1>
<div id="menu">
<ul>
  <li><a href="index.html">Volver al inicio</a></li></ul>
</div>

<div id="contenido">
<form action="registrar2.php" method="get">
  <p>Escriba su nombre de usuario y contraseña:</p>
  <table>
    <tbody>
      <tr>
        <td>Nombre:</td>
        <td><input type="text" name="usuario" size="20" maxlength="20" id="cursor" /> (hasta 20 caracteres)</td>
      </tr>
      <tr>
        <td>Contraseña:</td>
        <td><input type="password" name="password" size="20" maxlength="20" /> (hasta 20 caracteres)</td>
      </tr>
      <tr>
        <td>Repita la contraseña:</td>
        <td><input type="password" name="password2" size="20" maxlength="20" /> (hasta 20 caracteres)</td>
      </tr>
      </tbody>
  </table>
  <p><strong>Nota</strong>: Los nombres de más de 20 caracteres y las
  contraseñas de más de 20 se recortarán a esas longitudes.</p>
  <p><input type="submit" value="Añadir" />
  <input type="reset" value="Borrar" name="Reset" />
</form>
</div>

<div id="pie">
<address>
  Este programa esta hecho por Carlos Val Cebrian
</address>

</div>
</body>
</html>
